<?php

class SelectSort
{
      private $dados;

      public function __construct()
      {
           $this->dados = [30, 10, 40, 20, 50];
      }

      /**
       * Retorna o resultado do algoritmo de ordenação por inserção
       * como uma string
       *
       * @return string
       */
      public function resultSelectSort(): string
      {
            $result = '[';
            $order = $this->selectSort($this->dados);

            foreach ($order as $key => $dado) {
                if ($key == count($this->dados) - 1) {
                      $result .= $dado;
                      continue;
                }

                $result .= $dado . ', ';
            }

            $result .= ']';

            return $result;
      }

      /**
       * Método para ordenação por seleção
       *
       * @param  array  $dados
       *
       * @return array
       */
      private function selectSort(array $dados) : array
      {
            for ($i=0; $i < count($dados) - 1; $i++) {
                $menor = $dados[$i];

                for ($j=$i; $j < count($dados); $j++) {
                     if($menor > $dados[$j]) {
                          $menor = $dados[$j];
                          $indice = $j;
                     }
                }

                $dados[$indice] = $dados[$i];
                $dados[$i] = $menor;
            }

            return $dados;
      }
}
