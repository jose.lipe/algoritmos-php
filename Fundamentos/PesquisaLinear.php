<?php

class PesquisaLinear
{
      private $dados;

      public function __construct()
      {
            $this->dados= [22, 31, 77, 29, 30, 40, 78, 12, 7, 3];
      }

      /**
       * Método que efetua uma pesquisa linear em uma sequência de números
       *
       * @param  int    $value
       *
       * @return string
       */
      public function search(int $value) : string
      {
          for($i = 0; $i < count($this->dados); $i++) {
              if ($this->dados[$i] == $value) {
                  return (string) $this->dados[$i];
              }
          }

          return 'NULL';
      }
}
