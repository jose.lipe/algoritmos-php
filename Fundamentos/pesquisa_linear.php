<?php
    require __DIR__ . '/PesquisaLinear.php';

    $search = new PesquisaLinear;
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
    <title>Algoritmos PHP</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div class="container" style="margin-top: 2%">
        <div class="card">
          <div class="card-header">
            Ordenação por Inserção crescente
            <a class="btn btn-primary" href="../index.html" style="float: right;">Voltar</a>
          </div>
          <div class="card-body">
              <h5><b>Entrada de dados:</b></h5>
              <span>A = [22, 31, 77, 29, 30, 40, 78, 12, 7, 3]</span>
              <br><br>
              <span>Teste 1: V  = <b>77</b></span>
              <br>
              <span>Teste 1: V  = <b>5</b></span>

              <br><br>
              <h5><b>Saída esperada:</b></h5>
              <span>Teste 1: <b>77</b></span>
              <br>
              <span>Teste 2: <b>NULL</b></span>

              <br><br>
              <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" style="float: left;" class="mr-sm-2">
                <input type="hidden" name="teste" value="1">
                <button class="btn btn-success">Executar 1</button>
              </form>
              <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" style="float: left;">
                <input type="hidden" name="teste" value="2">
                <button class="btn btn-success">Executar 2</button>
              </form>

              <br><br>
              <div class="form-group">
                  <label>Resultado:</label>
                  <textarea class="form-control" style="background-color: #6c757d; color: white"><?php if (isset($_POST['teste'])) { echo $_POST['teste'] == 1 ? $search->search(77) : $search->search(5);} ?></textarea>
              </div>

          </div>
          <div class="card-footer text-muted">
              <a class="btn btn-secondary mr-sm-2" href="https://gitlab.com/jose.lipe/algoritmos-php/blob/master/Fundamentos/PesquisaLinear.php#L19" target="_blank" style="float: right">
                  <i class="fa fa-gitlab" aria-hidden="true" style="color: orange"></i>
                  &nbsp<span style="color: white">Códigos</span>
              </a>
          </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
