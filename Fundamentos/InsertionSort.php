<?php

/**
* Classe que contém os métodos de ordenação por inserção
*/
class InsertionSort
{
      private $dados;

      public function __construct()
      {
            $this->dados = [5, 2, 4, 6, 1, 3];
      }

      /**
       * Retorna o resultado do algoritmo de ordenação por inserção
       * como uma string
       *
       * @return string
       */
      public function resultInsertionSort(string $type = 'asc') : string
      {
          $result = '[';
          $order = array();

          if ($type == 'desc') {
              $order = $this->desc($this->dados);
          } else {
              $order = $this->asc($this->dados);
          }

          foreach ($order as $key => $dado) {
              if ($key == count($this->dados) - 1) {
                    $result .= $dado;
                    continue;
              }

              $result .= $dado . ', ';
          }

          $result .= ']';

          return $result;
      }

      /**
       * Método para ordenação por inserção crescente
       *
       * @param array $dados
       *
       * @return array
       */
      private function asc(array $dados) : array
      {
          for ($j = 1; $j < count($dados); $j++) {
              $chave = $dados[$j];
              $i = $j - 1;

              while ($i >= 0 && $dados[$i] > $chave) {
                  $dados[$i + 1] = $dados[$i];
                  $i = $i - 1;
              }

              $dados[$i + 1] = $chave;
          }

          return $dados;
      }

      /**
       * Método para ordenação por inserção decrescente
       *
       * @param array $dados
       *
       * @return array
       */
      private function desc(array $dados) : array
      {
            for($j = count($dados) - 2;  $j >= 0; $j--) {
                  $chave = $dados[$j];
                  $i = $j + 1;

                  while($i <= count($dados) - 1 && $dados[$i] > $chave) {
                      $dados[$i - 1] = $dados[$i];
                      $i = $i + 1;
                  }

                  $dados[$i - 1] = $chave;
            }

            return $dados;
      }
}
